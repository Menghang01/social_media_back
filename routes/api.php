<?php
if (App::environment('production')) {
    URL::forceScheme('https');
}
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FollowController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\ChatController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\LikeController;

Route::prefix('user')->group(function () {
    Route::get('/all', [UserController::class, 'index']);
});

Route::group(['prefix' => 'post'],function () {
    Route::get('', [PostController::class, 'getAll']);
    Route::delete('{id}', [PostController::class, 'delete']);
    Route::get('{id}', [PostController::class, 'getOne']);
});

Route::post('/createPost', [PostController::class, 'create']); 

Route::get('/search', [ProfileController::class, 'search']); 



Route::group(['prefix' => 'profile',  'middleware' => 'auth:sanctum'], function()
{
    Route::post('/{id}', [ProfileController::class, 'edit']);
    Route::get('/{userId}', [ProfileController::class, 'getOne']);  
});

Route::group(['prefix' => 'comment',  'middleware' => 'auth:sanctum'], function()
{
    Route::post('/{post_id}/{comment_id}', [CommentController::class, 'replyComment']);
    Route::post('/{post_id}', [CommentController::class, 'createComment']);  
});


Route::get('/profile/{userId}/post', [PostController::class, 'getUserPost']);

Route::get('/image/{id}', [ProfileController::class, 'getProfilePicture']);
Route::get('/image/posts/{id}', [PostController::class, 'getImage']);

Route::get('/follow/trending', [FollowController::class, 'getTrendingAccount']);

//public route
Route::post('/signup',[UserController::class,'store']);
Route::post('/login', [UserController::class, 'login']); 
Route::post('/logout', [UserController::class, 'logout']); 

Route::post('/broadcast',function (Request $request){
    $pusher = new Pusher\Pusher(env('PUSHER_APP_KEY'),env('PUSHER_APP_SECRET'), env('PUSHER_APP_ID'));
    return $pusher->socket_auth($request->request->get('channel_name'),$request->request->get('socket_id'));
});

//protected route
Route::middleware('auth:sanctum')->get('/users', function (Request $request) {
    return $request->user();
    
});

Route::middleware('auth:sanctum')->post('/chat/rooms ', [ChatController::class,'createRoom']);

Route::middleware('auth:sanctum')->get('/chat/rooms', [ChatController::class,'room']);
Route::middleware('auth:sanctum')->post('/chat/{roomId}', [ChatController::class,'getMessage']);
Route::middleware('auth:sanctum')->get('/chat/{rooms}/messages ', [ChatController::class,'messages']);
Route::middleware('auth:sanctum')->post('/chat/{rooms}/newMessage ', [ChatController::class,'newMessage']);


Route::middleware('auth:sanctum')->post('/follow/{id}', [FollowController::class, 'store']);
Route::middleware('auth:sanctum')->get('/follow/{id}', [FollowController::class, 'getFollowStatus']);

Route::get('/follow/{id}/data', [FollowController::class, 'getInfo']);
Route::get('/follow/{id}/follower', [FollowController::class, 'getFollower']);

Route::middleware('auth:sanctum')->get('/notifications', [NotificationController::class, 'get']);
Route::middleware('auth:sanctum')->get('/notifications/unread', [NotificationController::class, 'getUnread']);
Route::middleware('auth:sanctum')->post('/notifications/{id}', [NotificationController::class, 'markAsRead']);

Route::middleware('auth:sanctum')->post('/like/{id}', [LikeController::class, 'like']);

// Route::get('/notifications', [NotificationController::class, 'get']);


