<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Chatroom extends Model
{
    use HasFactory;
    public function messages(){
        return $this->hasMany(message::class);
    }
    public function users(){
        return $this->belongsToMany(User::class,'chatroom_users','chatroom_id', 'user_id'); 
    }
}
