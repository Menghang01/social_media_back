<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;
      protected $fillable = [
        'imageUrl',
        'caption', 
        'profile_id', 
    ];

    public function profile(){
        return $this->belongsTo(Profile::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class, 'post_id')->where('post_id', '!=', 0); 
    }

    public function likes()
    {
        return $this->belongsToMany(User::class);
    }
}
