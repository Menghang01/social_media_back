<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comment; 
use App\Models\User; 
use App\Models\Post; 
use App\Models\Profile; 
use App\Notifications\CommentNotification;

use Illuminate\Support\Facades\Auth;


class CommentController extends Controller
{
    public function createComment(Request $request, $post_id){
        $comment = new Comment(); 
        $comment->post_id = $post_id; 
        $comment->user_id = Auth::id();
        $comment->body = $request->body; 
        $comment->parent_id = 0; 

        $comment->save(); 
        $comment->parent_id = $comment->id; 

        $comment->save(); 

        $post = Post::find($post_id);
        $profile = Profile::find($post->profile_id);
        $authUser = Auth::user();
        
        if ($profile->user_id !== Auth::id()) {
            $profile->user->notify(new CommentNotification($comment, $authUser->profile, 'comment'));
        }
        
        return response("success",201); 
    }

    public function replyComment(Request $request, $post_id, $comment_id){
        $comment =  new Comment(); 
        $comment->parent_id = $comment_id; 
        $comment->body = $request->body;
        $comment->user_id = Auth::id();  
        $comment->post_id = 0; 
        $comment->save(); 

        $parentComent = Comment::find($comment_id);
        $user = User::find($parentComent->user_id);
        $authUser = Auth::user();
        
        if ($user->id !== Auth::id()) {
            $user->notify(new CommentNotification($comment, $authUser->profile, 'reply'));
        }

        return response("success",201); 
    }

    

}
