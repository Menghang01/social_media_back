<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use App\Models\User;

class NotificationController extends Controller
{
    //
    public function get() {
        $user = Auth::user();

        $notifications = $user->notifications;

        foreach($notifications as $noti) {
            $noti['difference_time'] = $noti->created_at->diffForHumans();
        }

        return response($notifications, 200);
    }

    public function getUnread() {
        $user = Auth::user();

        $notifications = $user->unreadNotifications;

        return response($notifications, 200);
    }

    public function markAsRead($id) {
        $user = Auth::user();

        $status = $user->notifications->where('id', $id)->markAsRead();

        return response($status, 200);
    }
}
