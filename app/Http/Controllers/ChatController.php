<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

Use App\Models\Chatroom;
use App\Events\Chat; 
Use App\Models\message;
Use App\Models\Profile;
Use App\Models\User;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use App\Notifications\ChatNotification;


class ChatController extends Controller
{

    public function createRoom(Request $request){
        $first_user =  User::findOrFail($request->first);
        $second_user =  User::findOrFail($request->second);

        $first_user_chatrooms = $first_user->chatRooms->makeHidden('pivot'); 
        $second_user_chatrooms = $second_user->chatRooms->makeHidden('pivot');  

        $result = $first_user_chatrooms->intersect($second_user_chatrooms);  

        
        if(count($result) == 0){
            $chatroom = new Chatroom(); 
            $chatroom->lastMessage = ""; 
            $chatroom->last_from = -1; 
            $chatroom->save(); 
            $first_user->chatRooms()->attach($chatroom->id); 
            $second_user->chatRooms()->attach($chatroom->id);
            return response([DB::table('chatroom_users')
            ->where('chatroom_users.chatroom_id',$chatroom->id)
            ->where('chatroom_users.user_id', '!=', Auth::id())
            ->join('chatrooms', 'chatroom_users.chatroom_id','=','chatrooms.id')
            ->join('profiles', 'chatroom_users.user_id', '=', 'profiles.user_id')
            ->first()]);  
        }
        
        return response([DB::table('chatroom_users')
        ->where('chatroom_users.chatroom_id',$result[0]->id)
        ->where('chatroom_users.user_id', '!=', Auth::id())
        ->join('chatrooms', 'chatroom_users.chatroom_id','=','chatrooms.id')
        ->join('profiles', 'chatroom_users.user_id', '=', 'profiles.user_id')
        ->first()]); 
    }

    public function room(Request $request){
        $filter_chatroom = array(); 
        $room = User::findOrFail(Auth::id())->chatRooms;

        foreach($room as $r){
            if(count($r->messages) > 0){
                array_push($filter_chatroom,$r); 
            }
        }
        $chatroom = array(); 
        foreach($filter_chatroom as $room){
            array_push($chatroom, DB::table('chatroom_users')
            ->where('chatroom_users.chatroom_id',$room->id)
            ->where('chatroom_users.user_id', '!=', Auth::id())
            ->join('chatrooms', 'chatroom_users.chatroom_id','=','chatrooms.id')
            ->join('profiles', 'chatroom_users.user_id', '=', 'profiles.user_id')
            ->first()
        ); 
        }
        return response($chatroom,200); 
    }

    public function messages(Request $request, $roomId ){
        return response(Chatroom::find($roomId)->messages,200);
    }   

    public function getMessage(Request $request, $roomId){
        $chatroom = Chatroom::findOrFail($roomId);
        $chatroom->isRead = 1; 
        $chatroom->save(); 
        return response("success",200);
    }

    public function newMessage(Request $request, $roomId){
        try{
            $newMessage = new message(); 
            $newMessage->from = Auth::id();
            $newMessage->chatroom_id = $roomId; 
            $newMessage->to = $request->to; 
            $newMessage->message = $request->message;
            $newMessage->isRead = 0;
            $newMessage->save(); 
            broadcast(new Chat($newMessage))->toOthers(); 

            $user = User::find($request->to);
            $authProfile = Auth::user();
            $user->notify(new ChatNotification($newMessage, $authProfile->profile));

            $chatroom = Chatroom::find($roomId); 
            $chatroom->lastMessage = $request->message; 
            $chatroom->isRead = "0"; 
            $chatroom->last_from = Auth::id(); 
            $chatroom->save(); 
           
            return response("success",200); 
        }
        catch(Throwable $e){
            return response($e); 
        }
     
    }

}
