<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash; 
use Illuminate\Support\Facades\Auth; 

use App\Models\User; 
use App\Models\Profile; 



class UserController extends Controller
{
    public function index(){
        // return new UserCollection(User::all()); 
        return User::all(); 
    }

    public function store(Request $request){
        $field = $request->validate([
            'name'=> 'required|string', 
            'email'=> 'required|string|unique:users,email', 
            'password'=> 'required|string|confirmed'
        ]);
        $user = User::create([
            'name' => $field['name'],
            'email' => $field['email'],
            'password' => bcrypt($field['password']) ]
        ); 
        $token = $user->createToken('kktoken')->plainTextToken; 
        $response = [
            'user' => $user,
            'token' => $token,
        ];

        // dd($user)
        $hello = Profile::create([
            'user_id' => $user->id, 
            'description' => "x", 
            'nickname' => $user->name, 
            'imageUrl' => "anomnymous.jpg"
        ]);

        $profile = Profile::findOrFail($user->id);
        
        return response($profile, 201); 
    }

    public function logout(Request $request){

        Auth::guard()->logout();

        // $request->session()->invalidate();

        // $request->session()->regenerateToken();
        // $request->user()->currentAccessToken()->delete();
        // Auth::guard('web')->logout();
        // auth()->logout; 

        return response()->json("logout", 204);
        
    }

    public function login(Request $request){
        $field = $request->validate([
            'email'=> 'required|string|', 
            'password'=> 'required|string|'
        ]);

        if(Auth::attempt($request->only('email', 'password'))){
            return response()->json(Auth::user()->profile,200);
        }

    }
}
