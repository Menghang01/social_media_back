<?php

namespace App\Http\Controllers;
use App\Models\Profile; 
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage; 


class ProfileController extends Controller
{

    public function getOne(Request $request , $userId){
        $profile = Profile::with('posts')->where('user_id', $userId)->get(); 
        return response($profile, 200); 
    }

    public function edit(Request $request, $userId){
        $profile = Profile::findOrFail($userId);
        if($request->hasFile('imageUrl')){
            $path = Storage::disk('digitalocean')->putFileAs('uploads',$request->file('imageUrl'), $request->file('imageUrl')->getClientOriginalName(),'public'); 
            $profile->fill($request->input())->save();  
            $profile->imageUrl = $request->file('imageUrl')->getClientOriginalName(); 
            $profile->save();
            return response($profile,201); 
        }
        else {
            $profile->fill($request->input())->save();
        } 
    }

    public function getProfilePicture(Request $request, $key){
        $file = Storage::disk('digitalocean')->get('uploads/' . $key);
        $headers = [
            'Content-Type' => 'image',
        ];
      
        return response($file, 200, $headers);
    }

    public function search(Request $request){
        $user = Profile::where('nickname','like','%' . $request->query('query') . '%')->get(); 
        return response($user);
    }
   
}
