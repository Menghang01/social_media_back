<?php

namespace App\Http\Controllers;
use App\Models\User; 
use App\Models\Profile; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\DB;

use App\Notifications\FollowNotification;

class FollowController extends Controller
{
    //
    public function store(Request $request, $userId) {
        $user = User::findOrFail($userId); 
        $status = auth()->user()->following()->toggle($user->profile);

        $authUser = Auth::user();

        if ($status["attached"] !== []) {
            $user->notify(new FollowNotification($authUser->profile, "follow"));
        } else if ($status["detached"] !== []) {
            $user->notify(new FollowNotification($authUser->profile, "unfollow"));
        }

        return response($status, 200);    
    }

    public function getFollowStatus(Request $request, $userId) {
        $follow = auth()->user() ? auth()->user()->following->contains($userId) : false;

        return response()->json([
            'status' => $follow,
        ]);    
    }

    public function getInfo(Request $request, $userId) {
        $user = User::findOrFail($userId); 

        $followerId = $user->profile->followers->pluck('id');

        $followers = Profile::whereIn('id', $followerId)->get(); 

        $followerCount = $followers->count();
        $followingCount = $user->following->count();

        return response()->json([
            'followers' => $followerCount,
            'following' => $followingCount,
            'data' => $followers
        ]);
    }

    public function getTrendingAccount(Request $request) {

        $topProfilesJoinQuery = DB::table('profile_user')
            ->select('profile_id', DB::raw('COUNT(profile_id) AS followers'))
            ->groupBy('profile_id');

        $top_profiles = DB::table('profiles')->select('*')
            ->join(DB::raw('(' . $topProfilesJoinQuery->toSql() . ') i'), function ($join)
            {
                $join->on('i.profile_id', '=', 'profiles.id');
            })
            ->orderBy('followers', 'desc')
            ->take(2)
            ->get();

        return response($top_profiles, 200);
    }
}
