<?php

namespace App\Http\Controllers;
use App\Models\Post; 
use App\Models\User; 
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage; 
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use App\Notifications\PostNotification;

class PostController extends Controller
{
    public function getAll(Request $request){
       
            $users = auth()->user()->following->pluck('user_id');
            $posts = Post::whereIn('profile_id', $users)->orWhere('profile_id', '=', Auth::id())->with(['profile', 'comments', 'comments.profile', 'comments.replies', 'comments.replies.profile'])->latest()->get();
            
            foreach($posts as $post) {
                $post['isLiked'] = $post->likes->contains(Auth::id());
                $post['likes'] = $post->likes->count();
                $post['difference_time'] = $post->created_at->diffForHumans();
                $post['comments'] = $post->comments; 
                foreach($post['comments'] as $c){
                    $c['difference_time'] = $c->created_at->diffForHumans(); 
                    foreach($c['replies'] as $reply){
                        $reply->difference_time = $reply->created_at->diffForHumans();
                    }
                }
    
            }

         
        
        
        
        return response($posts, 200);
    }

  
    public function getOne(Request $request, $id){
        $post = Post::with('profile')->where('id',$id)->first(); 
        $post['difference_time'] = $post->created_at->diffForHumans();
        return response($post,200); 
    }

    public function getUserPost(Request $request, $userId) {
        $posts = Post::where('profile_id', '=', $userId)->with('profile')->latest()->get();

        foreach($posts as $post) {
            $post['isLiked'] = $post->likes->contains(Auth::id());
            $post['likes'] = $post->likes->count();
            $post['difference_time'] = $post->created_at->diffForHumans();
            $post['comments'] = $post->comments; 
                foreach($post['comments'] as $p){
                    $p['profile'] = $p->profile;
                    $p['difference_time'] = $p->created_at->diffForHumans(); 
                    $p['replies'] = $p->replies; 
                    foreach($p['replies'] as $c){
                        $c['profile'] = $c->profile; 
                        $c['difference_time'] = $c->created_at->diffForHumans();
                    }
            }
        };
        

        return response($posts, 200);
    }

    public function create(Request $request){
        $authUser = Auth::user();
        $followers = $authUser->profile->followers;

        if($request->hasFile('imageUrl')){
    
        $path = Storage::disk('digitalocean')->putFileAs('posts',$request->file('imageUrl'), $request->file('imageUrl')->getClientOriginalName(),'public'); 

            $post = Post::create([
                'caption' => $request->caption, 
                'imageUrl' => $request->file('imageUrl')->getClientOriginalName(),
                'profile_id' => Auth::id()
            ]); 

            $feed = Post::findOrFail($post->id)->with(['profile', 'comments', 'comments.profile', 'comments.replies', 'comments.replies.profile'])->get();
            
            foreach($feed as $post) {
                $post['isLiked'] = $post->likes->contains(Auth::id());
                $post['likes'] = $post->likes->count();
                $post['difference_time'] = $post->created_at->diffForHumans();
                $post['comments'] = $post->comments; 
            }
 

            foreach($followers as $profile) {
                $user = User::find($profile->id);
                $user->notify(new PostNotification($authUser->profile, $post));
            }
            $authUser->notify(new PostNotification($authUser->profile, $post)); 

            return response($post, 201);
        
       }
       else {
           $post = Post::create([
               'caption' => $request->caption, 
               'profile_id' => Auth::id()
           ]);

           $feed = Post::findOrFail($post->id)->with(['profile', 'comments', 'comments.profile', 'comments.replies', 'comments.replies.profile'])->get();
            
           foreach($feed as $post) {
               $post['isLiked'] = $post->likes->contains(Auth::id());
               $post['likes'] = $post->likes->count();
               $post['difference_time'] = $post->created_at->diffForHumans();
               $post['comments'] = $post->comments; 
           }

           

           foreach($followers as $profile) {
               $user = User::find($profile->id);
               $user->notify(new PostNotification($authUser->profile, $post));
           }

           return response($post,201);
       }
    }

    public function delete(Request $request, $id){
        $post = Post::findOrFail($id); 

        Storage::disk('digitalocean')->delete('posts/' . $post->imageUrl);

        $post->delete();


        return response($post, 202);

    }
    
    public function getImage(Request $request, $key){
        $file = Storage::disk('digitalocean')->get('posts/' . $key);
        $headers = [
            'Content-Type' => 'video/mpeg, image',
        ];
        return response($file, 200, $headers);
 
    }


}
