<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use App\Models\Post;
use App\Models\User;

use App\Notifications\LikeNotification;


class LikeController extends Controller
{
    //
    public function like(Request $request, $postId) {
        $post = Post::findOrFail($postId);

        $status = auth()->user()->like()->toggle($post->id);

        $user = User::find($post->profile_id);
        $authUser = Auth::user();
        $count = $post->likes->count() - 1;
        
        if ($user->id !== Auth::id() && $status["attached"] !== []) {
            $user->notify(new LikeNotification($authUser->profile, $post, $count));
        }

        return response($status, 200);
    }
}
