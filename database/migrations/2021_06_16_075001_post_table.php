<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("posts", function(Blueprint $table){
            $table->id(); 
            $table->text("caption")->nullable(); 
            $table->string("imageUrl")->nullable(); 
            $table->integer('profile_id')->unsigned();
            $table->timestamps();
        }); 
    }

    /**
     * Reverse the migrations.ph
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
